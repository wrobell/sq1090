#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# ruff: noqa: SLF001

"""
Unit tests for data buffer structure with limited size.
"""

import asyncio
from sq1090 import DataBuffer

import pytest

@pytest.mark.asyncio
async def test_buffer() -> None:
    """
    Test data buffer structure.
    """
    # pylint: disable=protected-access

    buffer = DataBuffer[int](4)

    await buffer.put(1)
    await buffer.put(2)
    await buffer.put(3)
    await buffer.put(4)

    assert list(buffer._data) == [1, 2, 3, 4]

    # buffer blocks due to max_size == 4
    with pytest.raises(asyncio.TimeoutError) as ex_ctx:
        await asyncio.wait_for(buffer.put(9), 0.1)

    # nothing added due to timeout above
    assert list(buffer._data) == [1, 2, 3, 4]

    assert type(ex_ctx.value) is asyncio.TimeoutError

    async with buffer.get_all() as data:
        assert list(data) == [1, 2, 3, 4]

    assert len(buffer._data) == 0

    # ... and data can be added again
    await buffer.put(1)
    assert len(buffer._data) == 1

# vim: sw=4:et:ai
