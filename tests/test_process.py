#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Unit tests for Sq1090 data processing functions.
"""

from binascii import unhexlify
from collections.abc import Sequence

from sq1090.process import aircraft_data, callsign

import pytest

@pytest.fixture
def message() -> Sequence[bytes]:
    data: Sequence[str] = [
        '20000337202307',
        '8d4ca294601d139b36d1fa68e0f1',
        '8d4ca294201493b1dca820ae8d72',
        'a020041110010080e50000ac8afa',
        '8d392af9200464b6e144603515a9',
        '8d4ca294601d639b24d242290b87',
        '8d4ca67a204994b8c5a0e0e6fbc5',
        '0281839beff01c',
        '0261813c2f7bb4',
        '8d4caba7232422f1e32ce052abb7',
        '8d4ca4ee204994b6617820fc4896',
        'a8001bbeacf00031e400003b07c8',
        '8d4ca563204994b76038209ed25e',
        '8d4ca788201493b1d83820dbaeaa',
        '8d4caf89204d42f4cc4820625b98',
    ]
    return [unhexlify(v) for v in data]

def test_callsign(message: Sequence[bytes]) -> None:
    """
    Test processing of callsign data.
    """
    ts = list(range(len(message)))
    ad = aircraft_data(list(zip(ts, message)))
    result = callsign(ad)

    r_ts, r_icao, r_cat, r_type, r_cs = zip(*result)
    assert r_ts == (2, 4, 6, 9, 10, 12, 13, 14)
    icao_expected = (
        0x4ca294, 0x392af9, 0x4ca67a, 0x4caba7, 0x4ca4ee, 0x4ca563,
        0x4ca788, 0x4caf89
    )
    assert r_icao == icao_expected
    assert r_cat == (0, 0, 0, 3, 0, 0, 0, 0)
    assert r_type == (4, 4, 4, 4, 4, 4, 4, 4)

    cs_expected = (
        'EIN17J__', 'AFR68TQ_', 'RYR81ZC_', 'IBK1823_', 'RYR6XW__', 'RYR7XC__',
        'EIN16C__', 'STK43D__'
    )
    assert r_cs == cs_expected

# vim: sw=4:et:ai
