#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from datetime import datetime, UTC
from sq1090.util import parse_timestamp

import pytest

@pytest.mark.parametrize(
    'value, expected',
    [["2024-10-01", datetime(2024, 10, 1, tzinfo=UTC)],
     ["2024-10-01 00:00Z", datetime(2024, 10, 1, tzinfo=UTC)],
     ["2024-10-01 01:00:00+01:00", datetime(2024, 10, 1, tzinfo=UTC)]]
)
def test_parse_timestamp(value: str, expected: datetime) -> None:
    """
    Test parsing timestamp.
    """
    assert parse_timestamp(value) == expected

# vim: sw=4:et:ai
