#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
EWKB conversion functions.
"""

from functools import partial
from struct import Struct

# 3d position with srid 4326
as_point = partial(Struct('<BIIddd').pack, 1, 0xa0000001, 4326)

def to_ewkb(pos: tuple[float, float, float]) -> bytes:
    """
    Convert tuple (longitude, latitude, altitude) to WKB representation.
    """
    return as_point(*pos)

# vim: sw=4:et:ai
