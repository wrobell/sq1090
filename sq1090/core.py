#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Basic Sq1090 application API functions.
"""

import asyncio
import logging
from binascii import unhexlify
from collections.abc import AsyncIterator

logger = logging.getLogger(__name__)

async def read_messages(
        host: str='localhost',
        port: int=30002,
        delay: int=5,
) -> AsyncIterator[bytes]:
    """
    Read messages from ADS-B receiver.

    :param host: ADS-B receiver hostname.
    :param port: ADS-B receiver port.
    :param delay: Time in seconds to wait before reconnecting to ADS-B
        receiver.
    """
    while True:
        logger.info(
            'connecting to ads-b receiver: host={}, port={}'.format(host, port)
        )
        try:
            reader, _ = await asyncio.open_connection(host, port)
        except ConnectionRefusedError as ex:
            logger.info(
                'connection error (reconnect in {} sec): {}'.format(delay, ex)
            )
            await asyncio.sleep(delay)
        else:
            logger.info(
                'ads-b receiver connected: host={}, port={}'.format(host, port)
            )
            while True:
                msg = await reader.readline()
                if reader.at_eof():
                    logger.info(
                        'ads-b receiver disconnected: host={}, port={}'
                        .format(host, port)
                    )
                    break

                assert msg[:1] == b'*' and msg[-2:] == b';\n'
                yield unhexlify(msg[1:-2])

# vim: sw=4:et:ai
