#!/usr/bin/env python3
#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Receive ADS-B messages from a receiver, parse aircraft information from the
messages and store the data records in a database.

The aircraft data record is published onto `sq1090/record` topic of ZeroMQ socket.
"""

import argparse
import asyncio
import dataclasses as dtc
import logging
import n23
import n23.pub as n23p
import typing as tp

from n23.scheduler import TaskResult
from functools import partial

import sq1090
from sq1090.ewkb import to_ewkb
from sq1090.conf import read_conf

logger = logging.getLogger(__name__)

DATA_RECORDS = (sq1090.Callsign, sq1090.Position)

def add_entity(storage, name, cls, queue_size):
    """
    Add Sq1090 data record class as N23 storage entity.

    Map data record class to an entity, i.e. `Callsign` map as `callsign`
    or `Position` map as `position`.

    :param storage: N23 storage.
    :param cls: Sq1090 data record class.
    :param queue_size: N23 storage queue size.
    """
    cols = [f for f in cls.__dataclass_fields__]
    assert cols[0] == 'time'
    assert len(cols) > 1
    storage.add_entity(name, cols[1:], queue_size=queue_size)

def process(socket, storage, result: TaskResult):
    """
    Process received ADS-B message.

    The messages are received from N23 framework scheduler and are wrapped
    as task result objects.
    """
    record = sq1090.process(result.time, result.value)

    if record is None and __debug__:
        logger.debug('no record at this stage')
    elif record is not None and isinstance(record, DATA_RECORDS):
        if __debug__:
            logger.debug('data record received: {}'.format(record))

        #n23p.send_nowait(socket, 'sq1090/record', record)
        name = entity_name(record)
        record = dtc.replace(result, time=record.time, name=name, value=record)
        storage.write(record)

def entity_name(record) -> str:
    """
    Get Sq1090 data record entity name.
    """
    cls = record if isinstance(record, type) else record.__class__
    return cls.__name__.lower()

def as_tuple(result: TaskResult) -> tp.Tuple:
    """
    Extract Sq1090 data record as tuple.
    """
    return dtc.astuple(result.value)[1:]

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', default=False, action='store_true',
    help='show debug log'
)
parser.add_argument('conf', help='sq1090 config file')

args = parser.parse_args()

level = logging.DEBUG if args.verbose else logging.WARN
logging.basicConfig(level=level)

conf = read_conf(args.conf)

interval = 5
queue_size = 20000

with n23.run(interval, conf.db) as app, \
        sq1090.context(conf.location, conf.range):

    scheduler = app.scheduler

    storage = app.storage
    storage.set_type_codec('geometry', to_ewkb)
    add_result = partial(storage.add, transform=as_tuple)

    #socket = n23p.server('tcp://127.0.0.1:5557')
    socket = None

    callback = partial(process, socket, storage)
    scheduler.add('record', sq1090.read_messages, callback, regular=False)

    for cls in DATA_RECORDS:
        name = entity_name(cls)
        add_entity(storage, name, cls, queue_size)
        add_result(name)

# vim: sw=4:et:ai
