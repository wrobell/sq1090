#!/usr/bin/env Rscript
#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

library(dplyr)
library(ggplot2)
library(readr)
library(tidyr)

args = commandArgs(trailingOnly=T)

if (length(args) != 2) {
    cat('usage: plot-nucp-hist input output\n')
    quit('no', 1)
}

fin = args[1]
fout = args[2]

data = read_csv(fin, col_types=cols_only(time='T', hpl='d', rcu='d', rcv='d'))
data = data %>% pivot_longer(-time, names_to='nucp', values_to='value')

pdf(fout, width=6, height=12)

data = data %>% group_by(nucp, value) %>% count(value) %>% arrange(value)
data$value = factor(data$value)

p = (
    ggplot(data, aes(value, n))
    + facet_wrap(~nucp, ncol=1, scale='free')
    + geom_bar(stat='identity')
    + theme_bw()
)
p

dev.off()

# vim: sw=4:et:ai
