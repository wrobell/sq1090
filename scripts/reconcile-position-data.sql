--
-- Reconcile position data after position decoding.
--
-- 1. Create base position table: create table test_position as select * from position;
-- 2. Truncate position table.
-- 3. Run decoding of aircraft positions again.
-- 4. Reconcile with the query below.
-- 

with p_base as (
    select to_hex(icao) as icao, count(*), min(time) as start_time, max(time) as end_time
    from test_position
    group by 1
),
p_new as (
    select to_hex(icao) as icao, count(*), min(time) as start_time, max(time) as end_time
    from position
    group by 1
)
select
    pn.icao, pn.count - pb.count as count,
    pn.start_time - pb.start_time as start_time_diff,
    pn.end_time - pb.end_time as end_time_diff,
    pb.start_time, pb.end_time
from p_base pb full outer join p_new pn on pb.icao = pn.icao
where pb.count != pn.count
    or pb.count is null or pn.count is null
order by 1;
